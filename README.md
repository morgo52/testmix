# testmix

## Testing Types

### Functional
Which tests the real-world, business application of a software solution. For example, a ride-sharing app like Uber must be able to connect end users to drivers when all conditions are met, at the bare minimum.

### Non-functional
Which tests the remaining requirements of the software (for example performance, security, data storage, etc.) With the ride-sharing example, this type of testing will ensure that the app is fast and efficient when performing its most essential functions, like connecting end users to drivers in this case.

### Visual
Visual testing evaluates the visible output of an application and compares that output against the results expected by design. You can run visual tests at any time on any application with a visual user interface. Most developers run visual tests on individual components during development, and on a functioning application during end-to-end tests.

## Test Types

### Smoke Tests
Smoke tests are a type of Functional test that only covers the most crucial features of a software solution to ensure that it could be further tested without “catching fire,” hence the name Smoke Tests.

### Integration Tests
Integration tests take all the individual pieces and functionalities of a software solution and test them together as a whole to guarantee smooth operation between all of them.

### Regression Tests
Regression tests run a combination of Functional and Non-functional tests to check if the software has “regressed” after a given change.

### Security Tests
Security tests cover Functional and Non-functional tests that screen the software for any vulnerabilities. They reveal weaknesses and any potential exploit in a system.

### Perforamce Tests
Performance tests are often Non-functional tests that help testers evaluate criteria like responsiveness and stability as the software handles load and stress.

### Acceptance Tests
Acceptance tests are Functional tests that determine how acceptable the software is to the end users. This is the final test a solution must pass before it could be released.

## Phases of Testing

### Unit
As the name implies, this phase tests the individual components, or units, of a software. Unit testing is the very first phase of testing, usually done manually by developers before handing the software off to testers, but it could also be automated.

### API
Application Programming Interface (or API for short) acts as the “middleman” between all of the systems that your software uses, and thus, is then tested after the development process to ensure smooth integration between systems and software. This phase of testing is fairly flexible; it could be conducted either before or after the UI phase, which we will go over shortly, and by either the development or the testing team.

### UI
Last but not least, User Interface (AKA UI) is what the end users see and interact with and, thus, is usually tested at the very end of the process. This phase of testing is run by testers after the UI of the application has been drafted for the most authentic replication of user experience possible. This is where the business logic of the software is examined and optimized, which also falls under the Functional test classification.

### Component
Component Testing is a type of software testing in which usability of each individual component is tested. Along with the usability test, behavioral evaluation is also done for each individual component. To perform this type of testing, each component needs to be in independent state and also should be in controllable state. Each component of the software should be user comprehensible.

## Products

|               |   Unit    |   API |   UI      |   Component           |
|-              |-          |-      |-          |-                      |
|   Smoke       |           |       |   Cypress |   Cypress Component   |
|   Integration |           |       |   Cypress |   N/A?                |
|   Regression  |           |       |   Cypress |   N/A?                |
|   Security    |           |       |           |   N/A?                |
|   Performance |           |       |           |                       |
|   Acceptance  |           |       |   Cypress |   Cypress Component   |
          
